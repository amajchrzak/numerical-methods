# Numerical Methods

Realisation of projects
02.2020 - 06.2020

Repository of numerical methods laboratory

1. Bisection method
2. Newton-Raphson method
3. Hybrid method
4. Cubic inline interpolation
5. Gauss elimination
6. Lagrange interpolation
7. Monte Carlo integration
8. Numerical differentation
9. Numerical integration
10. False position and secant method
11. Richardson and Romberg schemes to calculate derivatives